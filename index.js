// console.log("Hello World");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function printWelcomeMessage(){
		let fullName = prompt("Enter Your Full Name");
		let age = prompt("Enter Your Age");
		let city = prompt("Enter The City You Live in");

		console.log ("Hello, "+ fullName);
		console.log ("You are "+ age + " years old.");
		console.log ("You live in "+ city);
		alert("Thank you for your input!");
	}

	printWelcomeMessage();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here: 

	function displayFaveBands(){

		console.log("1. Avril Lavigne");
		console.log("2. Incubus");
		console.log("3. Matchbox 20");
		console.log("4. Linkin Park");
		console.log("5. Halsey");
	}

	displayFaveBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function displayFaveMovies(){

		console.log("1. 500 Days of Summer");
		console.log("Rotten Tomatoes Rating: 85%");
		console.log("2. Frances Ha");
		console.log("Rotten Tomatoes Rating: 93%");
		console.log("3. Toy Story");
		console.log("Rotten Tomatoes Rating: 100%");
		console.log("4. John Wick");
		console.log("Rotten Tomatoes Rating: 86%");
		console.log("5. Baby Driver");
		console.log("Rotten Tomatoes Rating: 92%");
	}

	displayFaveMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/



// printUsers();
	let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");

	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");


	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();


// console.log(friend1);
// console.log(friend2);